<?php
include_once("modelos/vehiculo.php");
class Contrato{
    public $id_con;
    public $mod_c;
    public $col_c;
    public $añ_c;
    public $matri_c;
    public $apell_C;
    public $dni_c;
    public $fecha_i;
    public $fecha_f;

    public function __construct($id_con,$mod_c,$col_c,$añ_c,$matri_c,$apell_c,$dni_c,$fecha_i,$fecha_f){
        $this->id_con=$id_con;
        $this->mod_c=$mod_c;
        $this->col_c=$col_c;
        $this->añ_c=$añ_c;
        $this->matri_c=$matri_c;
        $this->apell_c=$apell_c;
        $this->dni_c=$dni_c;
        $this->fecha_i=$fecha_i;
        $this->fecha_f=$fecha_f;

    }

    public static function consultar(){
        $listaContratos=[];
        $conexionBDcon=BD::crearInstancia_con();
        $sql=$conexionBDcon->query("SELECT * FROM contratos");

        foreach($sql->fetchAll() as $contrato){
            $listaContratos[]=new Contrato($contrato['id_con'],$contrato['mod_c'],$contrato['col_c'],$contrato['añ_c'],$contrato['matri_c'],$contrato['apell_c'],$contrato['dni_c'],$contrato['fecha_i'],$contrato['fecha_f']);
        }
        return $listaContratos;
    }

    public static function crear($mod_c,$col_c,$añ_c,$matri_c,$apell_c,$dni_c,$fecha_i,$fecha_f){
        $conexionBDcon=BD::crearInstancia_con();
        $sql=$conexionBDcon->prepare("INSERT INTO contratos(mod_c,col_c,añ_c,matri_c,apell_c,dni_c,fecha_i,fecha_f) VALUES(?,?,?,?,?,?,?,?)");
        $sql->execute(array(Vehiculo::getModel('id_vehi'),Vehiculo::getColor('id_vehi'),Vehiculo::getAño('id_vehi'),Vehiculo::getMatri('id_vehi'),Cliente::getApellido('id_cli'),Cliente::getDNI('id_cli'),$fecha_i,$fecha_f));
    }
}
?>