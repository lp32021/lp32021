<?php

include_once("modelos/cliente.php");
include_once("conexion_c.php");


class ControladorClientes{
    public function inicio(){
        $clientes=Cliente::consultar();
        include_once("vistas/clientes/inicio.php");
    }
    public function crear(){
        if($_POST){
            print_r($_POST);
            $dni=$_POST['dni'];
            $nombres=$_POST['nombres'];
            $apellidos=$_POST['apellidos'];
            $edad=$_POST['edad'];
            $direccion=$_POST['direccion'];
            Cliente::crear($dni,$nombres,$apellidos,$edad,$direccion);
            header("Location:./?controlador=clientes&accion=inicio");
        }
        include_once("vistas/clientes/crear.php");
        
    }


    public function borrar(){
        $id_cli=$_GET['id_cli'];
        Cliente::borrar($id_cli);
        header("Location:./?controlador=clientes&accion=inicio");
    }
}
?>