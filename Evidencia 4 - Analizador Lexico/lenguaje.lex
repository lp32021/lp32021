package anlx;
import java_cup.runtime.*;
%%
%{
/*-*
* funciones y variables
*/
String example = "";
private void imprimir(String descripcion, String lexema) {
example = example + lexema + " ----> [ " + descripcion + " ] " + "\n";
}
%}
/*-*
* Informacion sobre la clase generada
*/
%public
%class AnalizadorLexico
%type void
/*-*
* Ajustes regulares
*/
BLANCO = [\n| |\t]
VARIABLE = [_|a-z|A-Z][a-z|A-Z|0-9|_]*
ENTERO = 0|[1-9][0-9]*
PUNTO_FLOTANTE = [0-9][0-9]*"."[0-9]*
COMENTARIO = "#"[[ ]*[[\\|'|,|!|#|$|%|&|/|(|)|=|?||[|]|.|;|:|{|}|+|\-|*|<|>|<=|>=|==|@]|[_|a-z|A-Z][a-z|A-Z|0-9|_]*|0|[1-9][0-9]*|[0-9][0-9]*"."[0-9]*]*[ ]*]*
STRING = "\""[[ ]*[[\\|'|,|!|#|$|%|&|/|(|)|=|?||[|]|.|;|:|{|}|+|\-|*|<|>|<=|>=|==|@]|[_|a-z|A-Z][a-z|A-Z|0-9|_]*|0|[1-9][0-9]*|[0-9][0-9]*"."[0-9]*][ ]*]*"\""
CARACTER = "\'"[[ ]*[[_|a-z|A-Z][a-z|A-Z|0-9|_]*|0|[1-9][0-9]*|[0-9][0-9]*"."[0-9]*][ ]*]*"\'"
ARRAY = "["[ ]*[["\'"[[ ]*[[_|a-z|A-Z][a-z|A-Z|0-9|_]*|0|[1-9][0-9]*|[0-9][0-9]*"."[0-9]*][ ]*]*"\'"]|[_|a-z|A-Z][a-z|A-Z|0-9|_]*|0|[1-9][0-9]*|[0-9][0-9]*"."[0-9]*][","[ ]*[["\'"[[ ]*[[_|a-z|A-Z][a-z|A-Z|0-9|_]*|0|[1-9][0-9]*|[0-9][0-9]*"."[0-9]*][ ]*]*"\'"]|[_|a-z|A-Z][a-z|A-Z|0-9|_]*|0|[1-9][0-9]*|[0-9][0-9]*"."[0-9]*]]*[ ]*"]"
OPERADORES_MATEMATICOS = ("+" | "-" | "*" | "/" | "**" | "%")
OPERADORES_ASIGNACION = ("+=" | "-=" | "*=" | "/=" | "**=" | "%=")
OPERADORES_LOGICOS = ("<" | ">" | "<=" | ">=" | "==" | "===" | "<=>" | "!=" | "!" | "||" | "&&")


%%
if | else | elsif { imprimir("Condicional", yytext()); }
while | for | until { imprimir("Bucle", yytext()); }
"=" { imprimir("Operador igual", yytext()); }
"{" { imprimir("Corchete Derecho", yytext()); }
"}" { imprimir("Corchete Izquierdo", yytext()); }
"(" { imprimir("Parentesis Derecho", yytext()); }
")" { imprimir("Parentesis Izquierdo", yytext()); }
"." { imprimir("Punto", yytext()); }
"\'"  { imprimir("Apostrofo", yytext()); }
","  { imprimir("Coma", yytext()); }
true | false { imprimir("Valor Booleano", yytext()); }
puts | print | nil | alias | break | case | length | end | rescue | module | super | self | when | then | unless | yield | _FILE_ | _LINE_ | next | redo | retry | return | class { imprimir("Palabra reservada", yytext()); }
and | or | not { imprimir ("Operador Logico", yytext()); }
{BLANCO} {}
{COMENTARIO} { imprimir("Comentario", yytext()); }
{VARIABLE} { imprimir("Identificador", yytext()); }
{ENTERO} { imprimir("Numero entero", yytext()); }
{PUNTO_FLOTANTE} { imprimir("Punto flotante", yytext()); }
{STRING} {imprimir("String",yytext()); }
{CARACTER} {imprimir("Caracter",yytext()); }
{ARRAY} { imprimir("Array", yytext()); }
{OPERADORES_MATEMATICOS} { imprimir("Operador Matematico", yytext()); }
{OPERADORES_ASIGNACION} { imprimir("Operador de asignacion", yytext()); }
{OPERADORES_LOGICOS} { imprimir("Operador logico", yytext()); }
. { throw new RuntimeException("Caracter invalido \""+yytext() +
"\" en la linea "+yyline+", columna "+yycolumn); }
