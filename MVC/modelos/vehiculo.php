<?php
class Vehiculo{
    public $id_vehi;
    public $matricula;
    public $modelo;
    public $color;
    public $marca;
    public $tipo;
    public $año;
    public $costo;

    public function __construct($id_vehi,$matricula,$modelo,$color,$marca,$tipo,$año,$costo){
        $this->id_vehi=$id_vehi;
        $this->matricula=$matricula;
        $this->modelo=$modelo;
        $this->color=$color;
        $this->marca=$marca;
        $this->tipo=$tipo;
        $this->año=$año;
        $this->costo=$costo;

    }

    public static function consultar(){
        $listaVehiculos=[];
        $conexionBDV=BD::crearInstancia_v();
        $sql=$conexionBDV->query("SELECT * FROM vehiculos");

        foreach($sql->fetchAll() as $vehiculo){
            $listaVehiculos[]=new Vehiculo($vehiculo['id_vehi'],$vehiculo['matricula'],$vehiculo['modelo'],$vehiculo['color'],$vehiculo['marca'],$vehiculo['tipo'],$vehiculo['año'],$vehiculo['costo']);
        }
        return $listaVehiculos;
    }

    public static function crear($matricula,$modelo,$color,$marca,$tipo,$año,$costo){
        $conexionBDV=BD::crearInstancia_v();
        $sql=$conexionBDV->prepare("INSERT INTO vehiculos(matricula,modelo,color,marca,tipo,año,costo) VALUES(?,?,?,?,?,?,?)");
        $sql->execute(array($matricula,$modelo,$color,$marca,$tipo,$año,$costo));
    }

    public static function borrar($id_vehi){
        $conexionBDV=BD::crearInstancia_v();
        $sql=$conexionBDV->prepare("DELETE FROM vehiculos WHERE id_vehi=?");
        $sql->execute(array($id_vehi));
    }
    public static function buscar($id_vehi){
        $conexionBDV=BD::crearInstancia_v();
        $sql=$conexionBDV->prepare("SELECT * FROM vehiculos WHERE id_vehi=?");
        $sql->execute(array($id_vehi));
        $vehiculo=$sql->fetch();
        return new Vehiculo($vehiculo['id_vehi'],$vehiculo['matricula'],$vehiculo['modelo'],$vehiculo['color'],$vehiculo['marca'],$vehiculo['tipo'],$vehiculo['año'],$vehiculo['costo']);

    }
    public static function getModel($id_vehi){
        $conexionBD=BD::crearInstancia_v();
        $sql=$conexionBD->prepare("SELECT * FROM vehiculos WHERE id_vehi=?");
        $sql->execute(array($id_vehi));
        $vehiculo=$sql->fetch();
        return ['modelo'];
    }
    public static function getMatri($id_vehi){
        $conexionBD=BD::crearInstancia_v();
        $sql=$conexionBD->prepare("SELECT * FROM vehiculos WHERE id_vehi=?");
        $sql->execute(array($id_vehi));
        $vehiculo=$sql->fetch();
        return ['matricula'];
    }
    public static function getAño($id_vehi){
        $conexionBD=BD::crearInstancia_v();
        $sql=$conexionBD->prepare("SELECT * FROM vehiculos WHERE id_vehi=?");
        $sql->execute(array($id_vehi));
        $vehiculo=$sql->fetch();
        return ['año'];
    }
    public static function getColor($id_vehi){
        $conexionBD=BD::crearInstancia_v();
        $sql=$conexionBD->prepare("SELECT * FROM vehiculos WHERE id_vehi=?");
        $sql->execute(array($id_vehi));
        $vehiculo=$sql->fetch();
        return ['color'];
    }

    public static function editar($id_vehi,$matricula,$modelo,$color,$marca,$tipo,$año,$costo){
        $conexionBDV=BD::crearInstancia_v();
        $sql=$conexionBDV->prepare("UPDATE vehiculos SET matricula=?, modelo=?, color=?, marca=?, tipo=?, año=?, costo=? WHERE id_vehi=?");
        $sql->execute(array($matricula,$modelo,$color,$marca,$tipo,$año,$costo,$id_vehi));

    }
}
?>