<?php
class Cliente{
    public $id_cli;
    public $dni;
    public $nombres;
    public $apellidos;
    public $edad;
    public $direccion;

    public function __construct($id_cli,$dni,$nombres,$apellidos,$edad,$direccion){
        $this->id_cli=$id_cli;
        $this->dni=$dni;
        $this->nombres=$nombres;
        $this->apellidos=$apellidos;
        $this->edad=$edad;
        $this->direccion=$direccion;
    }

    public static function consultar(){
        $listaClientes=[];
        $conexionBDC=BD::crearInstancia_c();
        $sql=$conexionBDC->query("SELECT * FROM clientes");

        foreach($sql->fetchAll() as $cliente){
            $listaClientes[]=new Cliente($cliente['id_cli'],$cliente['dni'],$cliente['nombres'],$cliente['apellidos'],$cliente['edad'],$cliente['direccion']);
        }
        return $listaClientes;
    }

    public static function crear($dni,$nombres,$apellidos,$edad,$direccion){
        $conexionBDC=BD::crearInstancia_c();
        $sql=$conexionBDC->prepare("INSERT INTO clientes(dni,nombres,apellidos,edad,direccion) VALUES(?,?,?,?,?)");
        $sql->execute(array($dni,$nombres,$apellidos,$edad,$direccion));
    }

    public static function borrar($id_cli){
        $conexionBDC=BD::crearInstancia_c();
        $sql=$conexionBDC->prepare("DELETE FROM clientes WHERE id_cli=?");
        $sql->execute(array($id_cli));
    }
    public static function getApellido($id_cli){
        $conexionBDC=BD::crearInstancia_c();
        $sql=$conexionBDC->prepare("SELECT * FROM clientes WHERE id_cli=?");
        $sql->execute(array($id_cli));
        $cliente=$sql->fetch();
        return $cliente['apellidos'];
    }
    public static function getDNI($id_cli){
        $conexionBDC=BD::crearInstancia_c();
        $sql=$conexionBDC->prepare("SELECT * FROM clientes WHERE id_cli=?");
        $sql->execute(array($id_cli));
        $cliente=$sql->fetch();
        return $cliente['dni'];
    }
    public static function buscar($id_cli){
        $conexionBDC=BD::crearInstancia_c();
        $sql=$conexionBDC->prepare("SELECT * FROM clientes WHERE id_cli=?");
        $sql->execute(array($id_cli));
        $cliente=$sql->fetch();
        return new Cliente($cliente['id_cli'],$cliente['id_cli'],$cliente['dni'],$cliente['nombres'],$cliente['apellidos'],$cliente['edad'],$cliente['direccion']);

    }

}
?>