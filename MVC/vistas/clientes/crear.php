<div class="card">
    <div class="card-header">
        Agregar Cliente
    </div>
    <div class="card-body">
        <form action="" method="post">
        <div class="mb-3">
          <label for="dni" class="form-label">DNI</label>
          <input required type="text"
            class="form-control" name="dni" id="dni" aria-describedby="helpId" placeholder="DNI del Cliente">
        </div>

        <div class="mb-3">
          <label for="nombres" class="form-label">Nombres</label>
          <input required type="text"
            class="form-control" name="nombres" id="nombres" aria-describedby="helpId" placeholder="Nombres del Cliente">
        </div>
        <div class="mb-3">
          <label for="apellidos" class="form-label">Apellidos</label>
          <input required type="text"
            class="form-control" name="apellidos" id="apellidos" aria-describedby="helpId" placeholder="Apellidos del Cliente">
        </div>
        <div class="mb-3">
          <label for="edad" class="form-label">Edad</label>
          <input required type="text"
            class="form-control" name="edad" id="edad" aria-describedby="helpId" placeholder="Edad del Cliente">
        </div>
        <div class="mb-3">
          <label for="direccion" class="form-label">Direccion</label>
          <input required type="text"
            class="form-control" name="direccion" id="direccion" aria-describedby="helpId" placeholder="Direccion del Cliente">
        </div>
        <input name="" id="" class="btn btn-success" type="submit" value="Añadir Cliente">
        </form>
        <a class="btn btn-primary" href="?controlador=clientes&accion=inicio" >Cancelar</a>
    </div>
</div>