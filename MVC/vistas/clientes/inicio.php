<a name="" id="" class="btn btn-success" href="?controlador=clientes&accion=crear" role="button">Añadir Cliente</a>

<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>DNI</th>
            <th>NOMBRES</th>
            <th>APELLIDOS</th>
            <th>EDAD</th>
            <th>DIRECCION</th>
            <th>ACCION</th>
        </tr>
    </thead>
    <tbody>
<?php foreach($clientes as $cliente){?>
        <tr>
            <td> <?php echo $cliente->id_cli;?></td>
            <td> <?php echo $cliente->dni;?> </td>
            <td> <?php echo $cliente->nombres;?> </td>
            <td> <?php echo $cliente->apellidos;?> </td>
            <td> <?php echo $cliente->edad;?> </td>
            <td> <?php echo $cliente->direccion;?> </td>
            <td>
                <div class="btn-group" role="group" aria-label="">
                    <a href="?controlador=clientes&accion=borrar&id_cli=<?php echo $cliente->id_cli;?>" class="btn btn-danger">Borrar</a>
                </div>
            </td>
        </tr>
<?php } ?>
    </tbody>
</table>
