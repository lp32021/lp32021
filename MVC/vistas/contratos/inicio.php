<a name="" id="" class="btn btn-success" href="?controlador=contratos&accion=crear" role="button">Crear un Contrato</a>

<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>MODELO</th>
            <th>COLOR</th>
            <th>AÑO</th>
            <th>MATRICULA</th>
            <th>APELLIDOS</th>
            <th>DNI</th>
            <th>FECHA INICIAL</th>
            <th>FECHA FINAL</th>
        </tr>
    </thead>
    <tbody>
<?php foreach($contratos as $contrato){?>
        <tr>
            <td> <?php echo $contrato->id_con;?></td>
            <td> <?php echo $contrato->mod_c;?> </td>
            <td> <?php echo $contrato->col_c;?> </td>
            <td> <?php echo $contrato->añ_c;?> </td>
            <td> <?php echo $contrato->matri_c;?> </td>
            <td> <?php echo $contrato->apell_c;?> </td>
            <td> <?php echo $contrato->dni_c;?> </td>
            <td> <?php echo $contrato->fecha_i;?> </td>
            <td> <?php echo $contrato->fecha_f;?> </td>
        </tr>
<?php } ?>
    </tbody>
</table>