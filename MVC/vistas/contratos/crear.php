<div class="card-header">
        Agregar Contrato
    </div>
    <div class="card-body">
        <form action="" method="post">
        <div class="mb-3">
          <label for="id_vehi" class="form-label">ID Vehiculo</label>
          <input required type="text"
            class="form-control" name="id_vehi" id="id_vehi" aria-describedby="helpId" placeholder="ID del Vehiculo">
        </div>

        <div class="mb-3">
          <label for="id_cli" class="form-label">ID Cliente</label>
          <input required type="text"
            class="form-control" name="id_cli" id="id_cli" aria-describedby="helpId" placeholder="ID del Cliente">
        </div>
        <div class="mb-3">
          <label for="fecha_i" class="form-label">Fecha Inicial</label>
          <input required type="date"
            class="form-control" name="fecha_i" id="fecha_i" aria-describedby="helpId" placeholder="Fecha Inicial">
        </div>
        <div class="mb-3">
          <label for="fecha_f" class="form-label">Fecha Final</label>
          <input required type="date"
            class="form-control" name="fecha_f" id="fecha_f" aria-describedby="helpId" placeholder="Fecha Final">
        </div>
        <input name="" id="" class="btn btn-success" type="submit" value="Añadir Contrato">
        </form>
        <a class="btn btn-primary" href="?controlador=contratos&accion=inicio" >Cancelar</a>
    </div>
</div>