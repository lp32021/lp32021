<div class="card">
    <div class="card-header">
        Agregar Vehiculo
    </div>
    <div class="card-body">
        <form action="" method="post">
        <div class="mb-3">
          <label for="matricula" class="form-label">Matricula</label>
          <input required type="text"
            class="form-control" name="matricula" id="matricula" aria-describedby="helpId" placeholder="Matricula del vehiculo">
        </div>

        <div class="mb-3">
          <label for="modelo" class="form-label">Modelo</label>
          <input required type="text"
            class="form-control" name="modelo" id="modelo" aria-describedby="helpId" placeholder="Modelo del vehiculo">
        </div>
        <div class="mb-3">
          <label for="color" class="form-label">Color</label>
          <input required type="text"
            class="form-control" name="color" id="color" aria-describedby="helpId" placeholder="Color del vehiculo">
        </div>
        <div class="mb-3">
          <label for="marca" class="form-label">Marca</label>
          <input required type="text"
            class="form-control" name="marca" id="marca" aria-describedby="helpId" placeholder="Marca del vehiculo">
        </div>
        <div class="mb-3">
          <label for="tipo" class="form-label">Tipo</label>
          <input required type="text"
            class="form-control" name="tipo" id="tipo" aria-describedby="helpId" placeholder="Tipo de vehiculo">
        </div>
        <div class="mb-3">
          <label for="año" class="form-label">Año</label>
          <input required type="number"
            class="form-control" name="año" id="año" aria-describedby="helpId" placeholder="Año de fabricacion del vehiculo">
        </div>
        <div class="mb-3">
          <label for="costo" class="form-label">Costo</label>
          <input required type="number"
            class="form-control" name="costo" id="costo" aria-describedby="helpId" placeholder="Costo del vehiculo por dia">
        </div>
        <input name="" id="" class="btn btn-success" type="submit" value="Añadir Vehiculo">
        </form>
        <a class="btn btn-primary" href="?controlador=vehiculos&accion=inicio" >Cancelar</a>
    </div>
</div>