<a name="" id="" class="btn btn-success" href="?controlador=vehiculos&accion=crear" role="button">Añadir Vehiculo</a>

<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>MATRICULA</th>
            <th>MODELO</th>
            <th>COLOR</th>
            <th>MARCA</th>
            <th>TIPO</th>
            <th>AÑO</th>
            <th>COSTO</th>
            <th>ACCION</th>
        </tr>
    </thead>
    <tbody>
<?php foreach($vehiculos as $vehiculo){?>
        <tr>
            <td> <?php echo $vehiculo->id_vehi;?></td>
            <td> <?php echo $vehiculo->matricula;?> </td>
            <td> <?php echo $vehiculo->modelo;?> </td>
            <td> <?php echo $vehiculo->color;?> </td>
            <td> <?php echo $vehiculo->marca;?> </td>
            <td> <?php echo $vehiculo->tipo;?> </td>
            <td> <?php echo $vehiculo->año;?> </td>
            <td> <?php echo $vehiculo->costo;?> </td>
            <td>
                <div class="btn-group" role="group" aria-label="">
                    <a href="?controlador=vehiculos&accion=editar&id_vehi=<?php echo $vehiculo->id_vehi;?>" class="btn btn-info">Editar</a>
                    <a href="?controlador=vehiculos&accion=borrar&id_vehi=<?php echo $vehiculo->id_vehi;?>" class="btn btn-danger">Borrar</a>
                </div>
            </td>
        </tr>
<?php } ?>
    </tbody>
</table>
