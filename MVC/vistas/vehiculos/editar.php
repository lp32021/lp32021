<div class="card">
    <div class="card-header">
        Editar datos de un Vehiculo
    </div>
    <div class="card-body">
        <form action="" method="post">

        <div class="mb-3">
          <label for="" class="form-label">ID</label>
          <input readonly type="text"
            class="form-control" value="<?php echo $vehiculo->id_vehi; ?> "name="id_vehi" id="" aria-describedby="helpId" placeholder="Id del Vehiculo">
        </div>

        <div class="mb-3">
          <label for="matricula" class="form-label">Matricula</label>
          <input required type="text"
            class="form-control" value="<?php echo $vehiculo->matricula; ?> " name="matricula" id="matricula" aria-describedby="helpId" placeholder="Matricula del vehiculo">
        </div>

        <div class="mb-3">
          <label for="modelo" class="form-label">Modelo</label>
          <input readonly type="text"
            class="form-control" value="<?php echo $vehiculo->modelo; ?> " name="modelo" id="modelo" aria-describedby="helpId" placeholder="Modelo del vehiculo">
        </div>
        <div class="mb-3">
          <label for="color" class="form-label">Color</label>
          <input required type="text"
            class="form-control" value="<?php echo $vehiculo->color; ?> " name="color" id="color" aria-describedby="helpId" placeholder="Color del vehiculo">
        </div>
        <div class="mb-3">
          <label for="marca" class="form-label">Marca</label>
          <input readonly type="text"
            class="form-control" value="<?php echo $vehiculo->marca; ?> " name="marca" id="marca" aria-describedby="helpId" placeholder="Marca del vehiculo">
        </div>
        <div class="mb-3">
          <label for="tipo" class="form-label">Tipo</label>
          <input readonly type="text"
            class="form-control" value="<?php echo $vehiculo->tipo; ?> " name="tipo" id="tipo" aria-describedby="helpId" placeholder="Tipo de vehiculo">
        </div>
        <div class="mb-3">
          <label for="año" class="form-label">Año</label>
          <input required type="text"
            class="form-control" value="<?php echo $vehiculo->año; ?> " name="año" id="año" aria-describedby="helpId" placeholder="Año de fabricacion del vehiculo">
        </div>
        <div class="mb-3">
          <label for="costo" class="form-label">Costo</label>
          <input required type="text"
            class="form-control" value="<?php echo $vehiculo->costo; ?> " name="costo" id="costo" aria-describedby="helpId" placeholder="Costo del vehiculo por dia">
        </div>
        <input name="" id="" class="btn btn-success" type="submit" value="Guardar Cambios">
        </form>
        <a class="btn btn-primary" href="?controlador=vehiculos&accion=inicio" >Cancelar</a>
    </div>
</div>