<?php

include_once("modelos/vehiculo.php");
include_once("conexion_v.php");


class ControladorVehiculos{
    public function inicio(){
        $vehiculos=Vehiculo::consultar();
        include_once("vistas/vehiculos/inicio.php");
    }
    public function crear(){
        if($_POST){
            print_r($_POST);
            $matricula=$_POST['matricula'];
            $modelo=$_POST['modelo'];
            $color=$_POST['color'];
            $marca=$_POST['marca'];
            $tipo=$_POST['tipo'];
            $año=$_POST['año'];
            $costo=$_POST['costo'];
            Vehiculo::crear($matricula,$modelo,$color,$marca,$tipo,$año,$costo);
            header("Location:./?controlador=vehiculos&accion=inicio");
        }
        include_once("vistas/vehiculos/crear.php");
        
    }
    public function editar(){
        
        if($_POST){
            $id_vehi=$_POST['id_vehi'];
            $matricula=$_POST['matricula'];
            $modelo=$_POST['modelo'];
            $color=$_POST['color'];
            $marca=$_POST['marca'];
            $tipo=$_POST['tipo'];
            $año=$_POST['año'];
            $costo=$_POST['costo'];
            Vehiculo::editar($id_vehi,$matricula,$modelo,$color,$marca,$tipo,$año,$costo);
            header("Location:./?controlador=vehiculos&accion=inicio");
            
        }
        $id_vehi=$_GET['id_vehi'];
        $vehiculo=(Vehiculo::buscar($id_vehi));
        include_once("vistas/vehiculos/editar.php");
        
    }

    public function borrar(){
        $id_vehi=$_GET['id_vehi'];
        Vehiculo::borrar($id_vehi);
        header("Location:./?controlador=vehiculos&accion=inicio");
    }
}
?>