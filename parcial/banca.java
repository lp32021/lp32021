package parcial;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;

public class banca {

	private JFrame frame;
	private JTable table;
	Double saldoT;
	public int estado=0;
	int ind = 1;
	DefaultTableModel modelo; 
	DecimalFormat df = new DecimalFormat("#.00"); //Formato de solo 2 decimales para el saldo.
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					banca window = new banca();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public banca() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Banca");
		frame.setBounds(100, 100, 716, 444);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnNewButton = new JButton("Cargar Archivo");
		
		JTextArea textArea = new JTextArea();
		
		JLabel lblNewLabel = new JLabel("Saldo Actual");
		
		JLabel myName = new JLabel("Felix David Prado Apaza - LP3");
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 680, Short.MAX_VALUE)
						.addComponent(btnNewButton)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(myName, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 187, Short.MAX_VALUE)
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(16)
					.addComponent(btnNewButton)
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 273, GroupLayout.PREFERRED_SIZE)
					.addGap(29)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(myName, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
						.addComponent(textArea, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(24, Short.MAX_VALUE))
		);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		frame.getContentPane().setLayout(groupLayout);
		textArea.setText(null);
		cuenta c1 = new cuenta();
		Random cantidad=new Random();
		Double monto;
		for(int i=1; i<=16;i++) {
			monto = (Double) (cantidad.nextDouble() * (cantidad.nextInt(2500)+1));
			Thread tt = new deposito(c1, monto);
			tt.start();
		}
		
		for(int i=1; i<=16;i++) {
			monto = (Double) (cantidad.nextDouble() * (cantidad.nextInt(2500)+1));
			Thread tt = new retiro(c1, monto);
			tt.start();
		}
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch(estado){
				case 0:
					if (ind==1) {
						saldoT = (double) (Math.round(c1.getSaldo()*100d)/100);
						ind=0;
					}
						textArea.append(Double.toString(saldoT));
					String rute = "C:\\Users\\Naria\\eclipse-workspace\\LP3-21B\\operaciones.csv";
					File file = new File(rute);
					try {
							BufferedReader br = new BufferedReader(new FileReader(file));
							String primeraLinea = br.readLine().trim();
							String[] nombreColumnas = primeraLinea.split(";");
							modelo = (DefaultTableModel)table.getModel();
							modelo.setColumnIdentifiers(nombreColumnas);
							
							Object[] lineaTabla = br.lines().toArray();
							
							for(int i = 0; i<lineaTabla.length; i++) {
								String linea = lineaTabla[i].toString().trim();
								String[] dataRow = linea.split(";");
								modelo.addRow(dataRow);
							
							}
					} catch(Exception ex) {
						
					}
					btnNewButton.setText("Reiniciar");
					estado = 1;
					break;
				case 1:
					modelo.getDataVector().removeAllElements();
					table.updateUI();
					textArea.setText(null);
					cuenta c2 = new cuenta();
					Random cantidad=new Random();
					Double monto;
					for(int j=1; j<=16;j++) {
						monto = (Double) (cantidad.nextDouble() * (cantidad.nextInt(2500)+1));
						Thread tt = new deposito(c2, monto);
						tt.start();
					}
					
					for(int j=1; j<=16;j++) {
						monto = (Double) (cantidad.nextDouble() * (cantidad.nextInt(2500)+1));
						Thread tt = new retiro(c2, monto);
						tt.start();
					}
					saldoT = 0.0;
					saldoT = (double) (Math.round(c2.getSaldo()*100d)/100);
					btnNewButton.setText("Cargar Archivo");
					estado = 0;

					break;
				}
			}
			
		});
		
	}
}
