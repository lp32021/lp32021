package parcial;

import java.util.Locale;

public class deposito extends Thread {

	cuenta cta;
	Double monto;
	
	public deposito(cuenta c, Double m) {
		this.cta = c;
		this.monto = m;
	}
	
	public void run() {
		
		try{
			this.cta.depositar(monto);
			//System.out.println(String.format(Locale.US, "%s acaba de depositar %,.2f", Thread.currentThread().getName(), monto));
		}catch(Exception e){
			System.out.println(e);
		}
		
	}
}
