package parcial;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class cuenta {
	
	//static PrintWriter oS;
	PrintWriter oS;
	double saldo=40000; //La cuenta empieza con 40000 inicial.
	String operacion;
	
	public cuenta(){
		try {
			oS = new PrintWriter("operaciones.csv"); //archivo csv donde se guarda las lineas de texto plano.
			this.oS.println("Operacion"+";"+"Monto"+";"+"Fecha"+";"+"Saldo Total"); //primera linea sera los nombres de las columnas
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		this.oS.flush();
		this.oS.close();
	}
	
	public synchronized void depositar (Double monto){
		
		try {
			this.oS = new PrintWriter(new FileOutputStream(new File("operaciones.csv"), true)); //se guarda la linea de texto en el archivo
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		operacion = "Deposito";
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSSSSS");
		Date date = new Date(); //la fecha se guarda con nanosegundos tambien
		saldo=saldo+monto; //se aplica el cambio al saldo (suma)
		DecimalFormat df = new DecimalFormat("#.00"); //se aplica un formato de decimales para que no sea demasiado grande el dato
		this.oS.println(operacion+";"+df.format(monto) +";"+dateFormat.format(date)+";"+df.format(saldo));
		this.oS.flush();
		this.oS.close();
		notify();
	}
	
	public synchronized void retirar (Double monto){
		try {
			this.oS = new PrintWriter(new FileOutputStream(new File("operaciones.csv"), true));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		operacion = "Retiro";
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSSSSS");
		Date date = new Date();
		DecimalFormat df = new DecimalFormat("#.00");
		saldo=saldo-monto;  //se aplica el cambio al saldo (resta)
		this.oS.println(operacion+";"+df.format(monto)+";"+dateFormat.format(date)+";"+df.format(saldo));
		this.oS.flush();
		this.oS.close();
		notify();
	}
	
	public Double getSaldo() {
		return this.saldo;
	}
	
	
	

}
