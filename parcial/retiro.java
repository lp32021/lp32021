package parcial;

import java.util.Locale;


public class retiro extends Thread{
	
	cuenta cta;
	Double monto;
	
	public retiro(cuenta c, Double m) {
		this.cta = c;
		this.monto = m;
	}
	
	public void run() {
		try{
			this.cta.retirar(monto);
			//System.out.println(String.format(Locale.US, "%s acaba de retirar %,.2f", Thread.currentThread().getName(), monto));
		}catch(Exception e){
			System.out.println(e);
		}
	}
	
}
